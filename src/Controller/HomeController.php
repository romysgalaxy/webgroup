<?php 

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Article;

class HomeController extends AbstractController
{
    /**
     * Injection de dépendances dans le constructeur
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Page d'accueil
     *
     * @return Response
     */
    public function index(): Response
    {        
        $repository = $this->doctrine->getRepository(Article::class);
        $articles = $repository->findByCreatedAt();

        return $this->render('home.html.twig', [
            'articles' => $articles
        ]);
    }

        
    /**
     * Page d'article
     *
     * @param string $slug
     * @return Response
     */
    public function show(string $slug): Response
    {
        $entityManager = $this->doctrine->getManager();
        $article = $entityManager->getRepository(Article::class)->findOneBy(['slug' => $slug]);

        return $this->renderForm('show.html.twig', [
            'article' => $article
        ]);   
    }
}