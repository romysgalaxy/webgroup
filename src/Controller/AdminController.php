<?php 

namespace App\Controller;

use App\Entity\Article;
use \DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry;
use App\Form\ArticleType;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AdminController extends AbstractController
{

    /** Répertoire pour récupérer les illustrations */
    public const UPLOAD_DIR = '/public/assets/upload'; 

    /**
     * Injection de dépendances dans le constructeur
     *
     * @param ManagerRegistry $doctrine
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Dashboard
     *
     * @return Response
     */
    public function index(): Response
    {
        $repository = $this->doctrine->getRepository(Article::class);
        $articles = $repository->findAll();
        
        return $this->render('admin/admin.html.twig', [
            'articles' => $articles
        ]);
    }
    
    /**
     * Création d'un nouvel article
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $article = new Article();
        $entityManager = $this->doctrine->getManager();
        $repository = $entityManager->getRepository(Article::class)->getLastArticleById();

        if (is_null($repository)) {
            $id = 1;
        } else {
            $id = $repository->getId() + 1;
        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        // Formulaire soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            
            if ($id < 10) {
                $id = '0' . $id;
            }
            $article->setSlug($id . '-' . strtolower(str_replace(' ', '-', $form['slug']->getData())));
            $article->setDateCreated(new DateTime());

            // Récupère l'illustration
            $file = $form['cover']->getData();
            if (!is_null($file) && ($file instanceof UploadedFile)) {
                // Télécharge l'illustration dans le répertoire et set la couverture de
                dump(dirname(__DIR__, 2));
                $file->move(dirname(__DIR__, 2) . self::UPLOAD_DIR, $file->getClientOriginalName());
                $article->setCover($file->getClientOriginalName());
            }

            try {
                $entityManager->persist($article);
                $entityManager->flush();
            } catch (Exception $e) {

                $this->addFlash(
                    'noticeException',
                    $e->getMessage()
                );

                return $this->renderForm('admin/create.html.twig', [
                    'form' => $form,
                    'article' => $repository,
                    'exception' => $e
                ]);
            }

            // Message de confirmation
            $this->addFlash(
                'notice',
                'Article crée !'
            );
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('admin/create.html.twig', [
            'form' => $form
        ]);

    }

    /**
     * Édition d'un article
     *
     * @param Request $request
     * @param integer $id
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        $entityManager = $this->doctrine->getManager();
        $repository = $entityManager->getRepository(Article::class)->find($id);
        $slug = $repository->getSlug();
        $cover = $repository->getCover();

        $form = $this->createForm(ArticleType::class, $repository);
        $form->handleRequest($request);

        // Formulaire soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();
            if ($id < 10) {
                $id = '0' . $id;
            }
            if ($slug !== $article->getSlug()) {
                $article->setSlug($id . '-' . strtolower(str_replace(' ', '-', $form['slug']->getData())));
            }
            // Récupère l'illustration
            $file = $form['cover']->getData();
            if (!is_null($cover) && is_null($file)) {
                // Empêche l'écrasement de l'illustration courante si aucune n'a été ajoutée
                $article->setCover($cover);
            } else if (!is_null($file) && ($file instanceof UploadedFile)) {
                // Télécharge l'illustration dans le répertoire et set la couverture de
                $file->move(dirname(__DIR__, 2) . self::UPLOAD_DIR, $file->getClientOriginalName());
                $article->setCover($file->getClientOriginalName());
            }

            try {
                $entityManager->persist($article);
                $entityManager->flush();
            } catch (Exception $e) {

                $this->addFlash(
                    'noticeException',
                    $e->getMessage()
                );

                return $this->renderForm('admin/edit.html.twig', [
                    'form' => $form,
                    'article' => $repository,
                    'exception' => $e
                ]);
            }

            // Message de confirmation
            $this->addFlash(
                'notice',
                'Article mis à jour !'
            );
            return $this->redirectToRoute('admin');
        }

        return $this->renderForm('admin/edit.html.twig', [
            'form' => $form,
            'article' => $repository
        ]);
    }
    
    /**
     * Suppression d'un article
     *
     * @param integer $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->doctrine->getManager();
        $article = $entityManager->getRepository(Article::class)->find($id);

        $entityManager->remove($article);
        $entityManager->flush();

        return $this->redirectToRoute('admin');
    }
}