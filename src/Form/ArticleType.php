<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\File\File;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('title', TextType::class, [
                    "label" => "Titre"
                ])
                ->add('slug', TextType::class, [
                    "label" => "Lien de l'article"
                ])
                ->add('content', TextareaType::class, [
                    "label" => "Contenu",
                    "attr" => [
                        "rows" => 10
                    ]
                ])
                ->add('cover', FileType::class, [
                    "data_class" => null,
                    "required"   => false,
                    "label" => "Télécharger une illustration de couverture",
                    
                ])
                ->add('Publier', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
