![alt text](https://gitlab.com/romysgalaxy/webgroup/-/raw/master/public/assets/images/cover.png)

Développement de l'application TEST de Webgroup

# Brief
https://webyzer.notion.site/TEST-Stage-f7a545adc559429884c3ba447fa39867

# Environnement
- `Windows avec WSL (Windows Subsystem for Linux)`
- `Docker v20.10.12`
- `Distribution Linux v20.04`

# Pré-requis
- `Symfony: v5.4`
- `PHP: v7.4`
- `MySQL: v5.7`
- `NodeJS: v12.12.12 `
- `Npm: v7.5.2`

Modifier la constante DATABASE_URL dans le fichier .env

# Commandes
- `sudo apt update`
- `sudo apt install nodejs`
- `sudo apt install npm`
- `composer install` Mise à jour des packages composer
- `php bin/console doctrine:database:create` Création de la base donnée
- `php bin/console doctrine:migration:migrate` Procède à la migration de la base de donnée
- `npm install` Mise à jour des packages npm 
- `npm run dev` Build du projet

# En cas de problème d'écriture dans les répertoires
- `sudo chown -R www-data:www-data ./var` Set le bon user pour l'écriture dans le répertoire /var
- `sudo chown -R www-data:www-data ./public` Set le bon user pour l'écriture dans le répertoire /public/assets/upload
